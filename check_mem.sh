#!/bin/bash

# Configure your limits and e-mail
RAM_LIMIT=10
SWAP_LIMIT=100
EMAIL='your@mail.com'

# Don't touch below these lines ;)
SWAP_FILE='/tmp/.swap_free'
PS_MEM_SCRIPT='/tmp/ps_mem.py'
PS_MEM_SCRIPT_URL='https://bitbucket.org/fernandes/memory_checker/raw/394675918db4a4a2f951b582b743a36e856bd705/ps_mem.py'
PS_MEM_SCRIPT_MD5='536c0e561f27fb80cd11a8309946259a'


# Initial check if we have mail and ps_mem.py to report memory usage
[ ! -f /bin/mail ] && echo "You need mail command to send alert" && exit 1

if [ ! -f ${PS_MEM_SCRIPT} ]; then
	wget -O ${PS_MEM_SCRIPT} ${PS_MEM_SCRIPT_URL}
	chmod +x ${PS_MEM_SCRIPT}
else
	md5_ps_mem_script=`md5sum ${PS_MEM_SCRIPT}|cut -d' ' -f1`
	if [ $md5_ps_mem_script != ${PS_MEM_SCRIPT_MD5} ]; then
		wget -O ${PS_MEM_SCRIPT} ${PS_MEM_SCRIPT_URL}
		chmod +x ${PS_MEM_SCRIPT}
	fi
fi

# Let's get the values and calculate the percentages
total_mem=`free -t|egrep "^Mem:"|awk -F" " '{print $2}'`
free_mem=`free -t|egrep "^Mem:"|awk -F" " '{print $4}'`
total_swap=`free -t|egrep "^Swap:"|awk -F" " '{print $2}'`
free_swap=`free -t|egrep "^Swap:"|awk -F" " '{print $4}'`

perc_free_mem=`echo "(${free_mem} / ${total_mem})*100"|bc -l| awk '{printf("%d\n", $1 * 1)}'`
perc_free_swap=`echo "(${free_swap} / ${total_swap})*100"|bc -l| awk '{printf("%d\n", $1 * 1)}'`

echo "Total Mem: ${total_mem} Free Mem: ${free_mem} (${perc_free_mem} %)"
echo "Total Swap: ${total_swap} Free Mem: ${free_swap} (${perc_free_swap} %)"

# If we are below the limit, send the mail reporting the memory usage
if [ $perc_free_mem -lt $RAM_LIMIT -a $perc_free_swap -lt $SWAP_LIMIT ]; then
	[ ! -f ${SWAP_FILE} ] && echo "${SWAP_LIMIT}" > ${SWAP_FILE}
	if [ `cat ${SWAP_FILE}` -gt $perc_free_swap ]; then
		${PS_MEM_SCRIPT} |mail -s "Memory Output for `hostname -f`" ${EMAIL}
		echo "$perc_free_swap" > ${SWAP_FILE} 
	fi
fi